
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author THANH HUNG
 */
public class ProductData {

    Product[] data;
    int used = 0;
    // khai báo mảng để chứa các product
    Scanner sc = new Scanner(System.in);

    public ProductData() {
        data = new Product[100];
        // lúc khởi tạo thì nó sẽ cho sẵn 100 phần tử
    }

    public void addNewProduct() {
        int ID;
        String name;
        String color = null;
        double price;
        System.out.println("==ADD NEW==");
        do {
            System.out.print("Input ID: ");
            ID = Validate.getAnInteger();
            if (isDuplicate(ID)) {
                System.out.println("ID is duplicate");
            }
        } while (isDuplicate(ID));

        System.out.print("Input name: ");
        name = sc.nextLine();
        System.out.print("Input color: ");
        color = sc.nextLine();
        System.out.print("Input price: ");
        price = Validate.getADouble();
        data[used] = new Product(ID, name, color, price);
        used++;
    }

    public void viewProduct() {
        System.out.println("List Product");
        System.out.printf("|%-3s|%-10s|%-10s|%-3s|\n", "ID", "Name", "Color", "Price");
        for (int i = 0; i < used; i++) {
            System.out.println(data[i].toString());
        }
    }

    public void findProduct() {
        int ID = 0;
        boolean found = false;
        System.out.print("Input ID need find: ");
        ID = Validate.getAnInteger();
        for (int i = 0; i < used; i++) {
            if (data[i].getID() == ID) {
                System.out.printf("|%-3s|%-10s|%-10s|%-3s|\n", "ID", "Name", "Color", "Price");
                System.out.println(data[i].toString());
                found = true;
                sc.nextLine(); // để người dùng dừng màn hình xem kết quả
                // lúc tìm ra thì chờ người dùng xem kết quả tìm thấy, sau khi xem thì ấn phím bất kì để tiếp tục chương trình
                return;
            }
        }
        if (!found) {
            System.out.println("Not found ID");
        }
    }

    public void updateProduct() {
        int ID = 0;
        String name;
        String color;
        double price;
        boolean found = false;
        System.out.print("Input ID need update: ");
        ID = Validate.getAnInteger();
        for (int i = 0; i < used; i++) {
            if (data[i].getID() == ID) {
                System.out.print("Input new name: ");
                name = sc.nextLine();
                data[i].setName(name);
                System.out.print("Input new color: ");
                color = sc.nextLine();
                data[i].setColor(color);
                System.out.print("Input new price: ");
                price = Validate.getADouble();
                data[i].setPrice(price);
                found = true;
                System.out.println("UPDATE SUCCESSFUL");
                sc.nextLine(); // để người dùng dừng màn hình xem kết quả
                return;
            }
        }
        if (!found) {
            System.out.println("Not found ID");
        }
    }

    boolean isDuplicate(int ID) {
        if (used == 0) {
            return false;
        }
        for (int i = 0 ; i < used ; i++) {
            if (data[i].getID() == ID) {
                return true;
            }
        }
        return false;
    }
}

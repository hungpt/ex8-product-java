/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANH HUNG
 */
public class Product {

    private int ID;
    private String name;
    private String color;
    private double price;

    public Product(int ID, String name, String color, double price) {
        this.ID = ID;
        this.name = name;
        this.color = color;
        this.price = price;
    }

    public Product() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("|%d|%-10s|%-10s|%.1f|", ID, name, color, price);
        // vì muốn xuất ra dạng bảng thì mỗi dòng đều phải có chung 1 format dạng cột
    }

}

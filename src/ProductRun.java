/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANH HUNG
 */
public class ProductRun {

    public static void main(String[] args) {
        int choice;
        ProductData dataProduct = new ProductData();
        // tạo 1 đối tượng array để chứa thông tin
        do {
            System.out.println("Product Management");
            System.out.println("1. Add new");
            System.out.println("2. View all");
            System.out.println("3. Find by ID");
            System.out.println("4. Update by ID");
            System.out.println("5. Exit!");
            System.out.print("Your choose: ");
            choice = Validate.getAnInteger();
            switch (choice) {
                case 1:
                    dataProduct.addNewProduct();
                    break;
                case 2:
                    dataProduct.viewProduct();
                    break;
                case 3:
                    dataProduct.findProduct();
                    break;
                case 4:
                    dataProduct.updateProduct();
                    break;
                case 5: return;
            }
        } while (choice != 5);
    }
}
